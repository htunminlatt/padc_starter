//
//  NewMovieDetailViewController.swift
//  Starter
//
//  Created by Sai Xtun on 24/03/2021.
//

import UIKit

class NewMovieDetailViewController: UIViewController {

    @IBOutlet weak var collectionViewCreators: UICollectionView!
    @IBOutlet weak var collectionViewActors: UICollectionView!
    @IBOutlet weak var imgBack: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgBack.isUserInteractionEnabled = true
        self.imgBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapBack)))
        registerCollectionView()
    }
    
    @objc func onTapBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
   //  setupcollectionview
    private func registerCollectionView() {
        self.collectionViewActors.delegate = self
        self.collectionViewActors.dataSource = self
        self.collectionViewActors.registerForCell(identifier: BestActorCollectionViewCell.identifier)

        self.collectionViewCreators.delegate = self
        self.collectionViewCreators.dataSource = self
        self.collectionViewCreators.registerForCell(identifier: BestActorCollectionViewCell.identifier)

    }
   
}

extension NewMovieDetailViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionViewActors {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BestActorCollectionViewCell.self), for: indexPath)as? BestActorCollectionViewCell else {
                return UICollectionViewCell()

        }
        return cell
            
        }else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BestActorCollectionViewCell.self), for: indexPath)as? BestActorCollectionViewCell else {
                return UICollectionViewCell()

        }
        return cell
        }
            

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2.5, height: 200)
    }

}
