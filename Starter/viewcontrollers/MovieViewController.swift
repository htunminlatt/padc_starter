//
//  MovieViewController.swift
//  Starter
//
//  Created by Sai Xtun on 24/01/2021.
//

import UIKit

protocol movieCellDelegate {
    func userDidTap()
}


class MovieViewController: UIViewController {

    @IBOutlet weak var ivSearch: UIImageView!
    @IBOutlet weak var ivMenu: UIImageView!
    @IBOutlet weak var viewForToolbar: UIView!
    @IBOutlet weak var tableViewMovies: UITableView!
    
    var delegate: movieCellDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpTableView()
    }

    
    func setUpTableView() {
        self.tableViewMovies.tableFooterView = UIView()
        self.tableViewMovies.delegate = self
        self.tableViewMovies.dataSource = self
        self.tableViewMovies.separatorStyle = .none

        self.tableViewMovies.registerForCell(identifier: MovieSliderTableViewCell.identifier)
        self.tableViewMovies.registerForCell(identifier: PopularFlimTableViewCell.identifier)
        self.tableViewMovies.registerForCell(identifier: MovieShowTimeTableViewCell.identifier)
        self.tableViewMovies.registerForCell(identifier: MovieTypeTableViewCell.identifier)
        self.tableViewMovies.registerForCell(identifier: ShowCaseTableViewCell.identifier)
        self.tableViewMovies.registerForCell(identifier: BestActorTableViewCell.identifier)


    }
    

}

extension MovieViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case MovieType.MOVIE_SLIDER.rawValue:
            let cell = tableView.dequeueCell(identifier: MovieSliderTableViewCell.identifier, indexPath: indexPath) as! MovieSliderTableViewCell
            cell.delegate = self
            return cell
            
        case MovieType.MOVIE_POPULAR.rawValue:
            let cell = tableView.dequeueCell(identifier: PopularFlimTableViewCell.identifier, indexPath: indexPath)as! PopularFlimTableViewCell
            cell.delegate = self
            return cell
            
        case MovieType.MOVIE_SHOWTIE.rawValue:
            return tableView.dequeueCell(identifier: MovieShowTimeTableViewCell.identifier, indexPath: indexPath)
            
        case MovieType.MOVIE_GENRE.rawValue:
            return tableView.dequeueCell(identifier: MovieTypeTableViewCell.identifier, indexPath: indexPath)
            
        case MovieType.MOVIE_SHOWCASE.rawValue:
            return tableView.dequeueCell(identifier: ShowCaseTableViewCell.identifier, indexPath: indexPath)
            
        case MovieType.MOVIE_BESTACTOR.rawValue:
            return tableView.dequeueCell(identifier: BestActorTableViewCell.identifier, indexPath: indexPath)
        default:
            return UITableViewCell()
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 220
        case 1:
            return 290
        case 2:
            return 160
        case 3:
            return 340
        case 4:
            return 210
        case 5:
            return 245
        default:
            return 0
        }
    }
    
}

extension MovieViewController: movieCellDelegate,MovieItemDelegate {
    func onTapItem() {
//        self.navigateToMovieDetailViewController()
        self.navigateToNewMovieDetailViewcontroller()
    }
    
    func userDidTap() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: String(describing: MovieDetailViewController.self)) as! MovieDetailViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
}
