//
//  MovieType.swift
//  Starter
//
//  Created by Sai Xtun on 31/01/2021.
//

import Foundation
import UIKit

enum MovieType: Int {
    case MOVIE_SLIDER = 0
    case MOVIE_POPULAR = 1
    case MOVIE_SHOWTIE = 2
    case MOVIE_GENRE = 3
    case MOVIE_SHOWCASE = 4
    case MOVIE_BESTACTOR = 5


}
