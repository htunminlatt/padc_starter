//
//  GenreVO.swift
//  Starter
//
//  Created by Sai Xtun on 30/01/2021.
//

import Foundation

class GenreVO {
    var name : String = "Action"
    var isSelected : Bool = false
    
    init(name: String,isSelected: Bool) {
        self.name = name
        self.isSelected = isSelected
         
    }
}
