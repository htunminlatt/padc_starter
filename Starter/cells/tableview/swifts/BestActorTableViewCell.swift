//
//  BestActorTableViewCell.swift
//  Starter
//
//  Created by Sai Xtun on 24/01/2021.
//

import UIKit

class BestActorTableViewCell: UITableViewCell {
    @IBOutlet weak var bestActorCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpCollectionView()
    }
    
    //setup collectionview
    func setUpCollectionView() {
        self.bestActorCollectionView.delegate = self
        self.bestActorCollectionView.dataSource = self
        self.bestActorCollectionView.showsHorizontalScrollIndicator = false
                
        self.bestActorCollectionView.registerForCell(identifier: BestActorCollectionViewCell.identifier)
        
        let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 15
            layout.minimumInteritemSpacing = 15
        self.bestActorCollectionView.setCollectionViewLayout(layout, animated: true, completion: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension BestActorTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeCell(identifier: BestActorCollectionViewCell.identifier, indexPath: indexPath)as! BestActorCollectionViewCell
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 200)
    }
}

extension BestActorTableViewCell: ActorActionDelegate {
    func onTapFavourite(isFavourite: Bool) {
        print("favourite.....\(isFavourite)")
    }
    
    
}

