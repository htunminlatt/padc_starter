//
//  PopularFlimTableViewCell.swift
//  Starter
//
//  Created by Sai Xtun on 24/01/2021.
//

import UIKit

class PopularFlimTableViewCell: UITableViewCell {

    @IBOutlet weak var popularFlimCollectionView: UICollectionView!
    
    var delegate : movieCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        self.setUpCollectionView()
    }
    
    //setup collectionview
    func setUpCollectionView() {
        self.popularFlimCollectionView.delegate = self
        self.popularFlimCollectionView.dataSource = self
        self.popularFlimCollectionView.showsHorizontalScrollIndicator = false
        
        
        self.popularFlimCollectionView.registerForCell(identifier: PopularFlimCollectionViewCell.identifier)
        
        let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 15
            layout.minimumInteritemSpacing = 15
        self.popularFlimCollectionView.setCollectionViewLayout(layout, animated: true, completion: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}

extension PopularFlimTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return collectionView.dequeCell(identifier: PopularFlimCollectionViewCell.identifier, indexPath: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 255)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.userDidTap()
    }
    
    
}

