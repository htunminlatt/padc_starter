//
//  MovieTypeTableViewCell.swift
//  Starter
//
//  Created by Sai Xtun on 24/01/2021.
//

import UIKit

class MovieTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var typeCollectionView: UICollectionView!
    
    @IBOutlet weak var movieCollectionView: UICollectionView!
    
    var selectedIndex : Int = 0
    
    var movieTypeArray = ["ACTION","ADVANTURE","CRIMINAL","DRAMA","COMEDY"]
    
    var genreList = [GenreVO(name: "ACTION", isSelected: true),GenreVO(name: "ADVANTURE", isSelected: false),GenreVO(name: "CRIMINAL", isSelected: false),GenreVO(name: "DRAMA", isSelected: false),GenreVO(name: "COMEDY", isSelected: false)]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpCollectionView()
    }
    
    //setup collectionview
    func setUpCollectionView() {
        self.typeCollectionView.delegate = self
        self.typeCollectionView.dataSource = self
        self.typeCollectionView.showsHorizontalScrollIndicator = false
        
        
        self.typeCollectionView.registerForCell(identifier: TypeCollectionViewCell.identifier)
        
        let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        self.typeCollectionView.setCollectionViewLayout(layout, animated: true, completion: nil)
        
        self.movieCollectionView.delegate = self
        self.movieCollectionView.dataSource = self
        self.movieCollectionView.showsHorizontalScrollIndicator = false
        
        self.movieCollectionView.registerForCell(identifier: PopularFlimCollectionViewCell.identifier)
        
        let layout2 = UICollectionViewFlowLayout()
            layout2.scrollDirection = .horizontal
            layout2.minimumLineSpacing = 15
            layout2.minimumInteritemSpacing = 15
        self.movieCollectionView.setCollectionViewLayout(layout2, animated: true, completion: nil)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension MovieTypeTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.typeCollectionView {
            return self.genreList.count

        }else {
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.typeCollectionView {
            let cell = collectionView.dequeCell(identifier: TypeCollectionViewCell.identifier, indexPath: indexPath)as! TypeCollectionViewCell
            
            cell.data = self.genreList[indexPath.row]
            cell.onTapItem = {generName in
                self.genreList.forEach { (genreVo) in
                    if generName == genreVo.name {
                        genreVo.isSelected = true
                    }else {
                        genreVo.isSelected = false
                    }
                }
                self.typeCollectionView.reloadData()
            }
            
            return cell
        }else {
            let cell = collectionView.dequeCell(identifier: PopularFlimCollectionViewCell.identifier, indexPath: indexPath)
            cell.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.1178759989, blue: 0.2665614298, alpha: 1)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.typeCollectionView {
            return CGSize(width: self.widthForString(text: self.genreList[indexPath.row].name, font: UIFont(name: "Geeza Pro", size: 14) ?? UIFont.systemFont(ofSize: 14)) + 20, height: 50.0)
        }else
        {
            return  CGSize(width: 150, height: 255)
        }
    }
    
    func widthForString(text: String,font: UIFont) -> CGFloat {
        let fontAttribute = [NSAttributedString.Key.font : font]
        let textSize = text.size(withAttributes: fontAttribute)
        return textSize.width
    }
    
}
